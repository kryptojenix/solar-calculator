#!/usr/bin/python3
"""
loads the excel and saves the excel worksheet
creates a dataframe from the named range in the excel spreadsheet
fills missing items in the dataframe
retreives website links & maintains cached versions
creates pygrab-buy store objects and uses their methods to scrape missing info
gets user input for category and part-code fields

"""
from urllib.request import Request, urlopen
from urllib.error import HTTPError
from urllib.error import URLError
import urllib.request
import os
#from PIL import Image

from bs4 import BeautifulSoup

import pandas as pd

import openpyxl #import Workbook load_workbook

# define the baseDir and load config
#dataName = "clientdata"
baseDir = os.path.realpath(__file__).split("update_price_list.py")[0]
#dataDir = os.path.join(baseDir, dataName)
pluginDir = os.path.join(baseDir, "plugins")
localDir = os.path.join(pluginDir, "update_price_list")

testing = True

#TODO: get current client's directory?


def main():
    print("main")

    
def update_old():
    pass
def update():
    #print("Loading")
    #workBook = openpyxl.load_workbook(priceListFile)
    
    print("Updating")

    sheet = "parts"
    dataRange = 3,85,8     #rowStart, rowEnd, colEnd
    try:
        excelFile = pd.ExcelFile(os.path.join(baseDir, 'clientdata', select()))
        sheet = "parts"
        dataFrame = excelFile.parse(sheet)
        
        
    except Exception as e:
        print("Problem loading workBook: %s" % e)

    print("dataFrame status : %s" % dataFrame)
    
    
    
    
    #create and update Item object
    createItems = True
    if createItems :
        items = dataFrame.keys
        print(items)
        
        
        for item in items:
            print(item)
            result = item.get_link()
            
            soup = BeautifulSoup(result, features="lxml")
            #print(soup)
            print("******* end of soup *********")
            
            # make sure the plugin directories are set up
            ok = check_supplier_cache(item.supplier)
            if ok:
                print("item.update_price")
                item.save_html(str(soup))
                item.update_price(soup)
            else:
                print("Cannot save html")
            
            
            
            if item.supplier == "jaycar":
                print("doing Jaycar scrape")
                
            elif item.supplier == "ebay":
                print("doing eBay scrape")
                
            else:
                print("cannot scrape this supplier: %s" % item.supplier)
            
            
            
            
            
        
        
    print("scrape done.")
        
    # do the write
        

def write_range(workSheet, dataRange, dataFrame):
    """
    writes updated dataframe back to the spreadsheet
    accepts: worksheet, datarange, dataframe
    returns: 
    """
    # check range
    # TODO: this  
    print(dataRange)
    rowStart = 3
    rowEnd = rowStart + 32
    colEnd = "H"
    dataRange=(rowStart,rowEnd,colEnd)
    print("write_range: %s to %s" % (dataRange[0],dataRange[1]))
        

    try:
        for row in range(dataRange[0], dataRange[1]):
            #TODO: write back the pandas dataframe
            pass
            
    except Exception as e:
        print("problem reading parts sheet: %s" % e)
            
    except Exception as e:
        print("range error : %s" % e)

def read_range_old(workSheet, dataRange):
    """
    returns a list of item objects
    item[code,link,supplier,]
    """
    print("read_range: %s to %s" % (dataRange[0],dataRange[1]))
    try:
        items = []
        for row in range(dataRange[0],dataRange[1]): 
            #read code
            cellRef = 'A' + str(row)    
            code = workSheet[cellRef].value
            
            #read link
            cellRef = 'B' + str(row)    
            link = workSheet[cellRef].value
            
            cellRef = 'C' + str(row)    
            supplier = workSheet[cellRef].value
            
            # don't add empty rows
            if code == link == supplier == None:
                pass
            else:
                #item = (code,link,supplier)
                itemObject = Item(code,link,supplier)
                items.append(itemObject)
            
            
    except Exception as e:
        print("problem reading parts sheet: %s" % e)
            
    except Exception as e:
        print("range error : %s" % e)
    
    
    return items
    

def read_range(workSheet, dataRange):
    """
    returns a pandas dataframe
    creates an Item object: item[code,link,supplier,price,etc]
    """
    print("read_range: %s to %s" % (dataRange[0],dataRange[1]))
    try:
        items = []
        for row in range(dataRange[0],dataRange[1]): 
            #read code
            cellRef = 'A' + str(row)    
            code = workSheet[cellRef].value
            
            #read link
            cellRef = 'B' + str(row)    
            link = workSheet[cellRef].value
            
            cellRef = 'C' + str(row)    
            supplier = workSheet[cellRef].value
            
            # don't add empty rows
            if code == link == supplier == None:
                pass
            else:
                #item = (code,link,supplier)
                itemObject = Item(code,link,supplier)
                items.append(itemObject)
            
            
    except Exception as e:
        print("problem reading parts sheet: %s" % e)
            
    except Exception as e:
        print("range error : %s" % e)
    
   
    
    return items





if __name__ == "__main__":
    print("This file should be imported")
    main()
else:
    print("imported %s" % __file__)
    #main()
