#!/usr/bin/python3

import os
import openpyxl #import Workbook load_workbook 
import pandas as pd

#solar-calculator library scripts
import bom_tally
import update_price_list

# define the baseDir and load config
projectName = "solar-calculator"
dataName = "clientdata"
baseDir = os.path.realpath(__file__).split("solar-calculator.py")[0]
dataDir = os.path.join(baseDir, dataName)
pluginDir = os.path.join(baseDir, "plugins")

# some defaults
defaultTemplate = os.path.join(baseDir, "solar-calculator_template.xlsx")
defaultClient = "client"
defaultPricelist = os.path.join(baseDir, "clientdata", "pricelist.xlsx")
# TODO: run update_pricelist.sh

# enable plugins
do_bom_tally = False
do_update_price_list = True


# run spreadsheet calcs
do_update_latitude = False
do_calculate_sun_angle = False

def main():
    print(__name__)




def start_new_session():
    """
    Asks for a client and returns a workbook.  Automagically creates new client or loads existing client.
    """
    clientName = get_client_name(defaultClient)
    print("Got client name: %s" % clientName)
    fileString = "solar-calculator_" + clientName + ".xlsx"
    clientPath = os.path.join(dataDir, clientName)
    clientFile = os.path.join(dataDir, clientName, fileString)

    # existing client?
    existing = False
    if os.path.isdir(clientPath):
        print("client path exists")
        
        try:
            workBook = openpyxl.load_workbook(clientFile)
            print("File exists: %s" % clientFile)
            existing = True
        except Exception as e:
            print("coluld not load existing workbook because: %s" % e)
            #load default template
    else:
        print("Creating new client: %s" % clientName)
        os.makedirs(clientPath)
        ok = True
        
        
    if existing:
        print("Loaded client workbook")
        ok = True
    else:
        print("loading workbook template")
        workBook = openpyxl.load_workbook(defaultTemplate)
        workBook.save(filename = clientFile)
    
    # check for latitude, input if it isn't there
    sheet = "PV_Yearly"
    
    workSheet = workBook.get_sheet_by_name(sheet)
    cellRef = "B16"
    print(workSheet[cellRef].value)
    if workSheet[cellRef].value == "":
        workSheet[cellRef].value == "-20"
    
        
    
    
    return(clientPath, fileString, workBook)


def get_client_name(defaultClient):
    cName = input("Enter new client name [%s]: " % defaultClient)
    #TODO: return filesystem friendly name
    if cName == "":
        cName = defaultClient
    
    return cName


def create_new_client(clientName):
    fileString = "solar-calculator_" + clientName + ".xlsx"
    clientPath = os.path.join(dataDir, clientName)
    clientFile = os.path.join(dataDir, clientName, fileString)
    success = False
    try:
        os.makedirs(clientPath)
        workBook = openpyxl.load_workbook(defaultTemplate) 
        success = True
    except Exception as e:
        print(e)
    
    
    
    if success:
        try:
            workBook.save(filename = clientFile)
        except Exception as e:
            print("could not save workbook at %s" % clientFile)
            print("because %s " % e)
    else:
        print("There was an error setting up cleint directory")


    if success:
    #if os.path.isdir(clientPath):
        #create .xlsx file
        try:
            if os.path.isfile(clientFile):
                print("file exists, not making a backup")
                #TODO: make a backup
                
            else:
                print("creating file")
                try:
                    workBook.save(filename = clientFile)
                except Exception as e:
                    print("could not create file because: %s" % e)
                    success = False
        except Exception as e:
            print("file status is: %s" % e)
            success = False
    else:
        print("something went wrong")
    return success


def save_workbook(workBook):
    #add revision notes & save
    text = input("comments: ")
    # save workbook    
    print("Saving file")

def input_latitude():
    """
    returns latitude
    """
    print("setting latitude to -20")
    latitude = -20
    return latitude
    #pass
    
def calculate_sun_angle(lat, day, month):
    angles = []
    latOffset = -5
    dawn, dusk = 6, 18
    for hour in range(dawn,dusk,1):
        daylight = 180 + 2 * latOffset
        degrees = latOffset + (daylight/(dusk-dawn)*hour)
        #print("%s:00       sun angle = %s" % (hour, degrees))
        angles.append((hour,degrees))
        
    
    return angles




def forgotten():
    """
    forgotten code

    """
    pass
    #try:
        #workBook = openpyxl.load_workbook(clientFile)
    #except Exception as e:
        #print("load workbook failed because: %s" % e)
        


    #test = create_new_client(clientName)



    # check for expected paths & load default templates
    #fileString = "solar-calculator_" + clientName + ".xlsx"
    #clientPath = os.path.join(dataDir, clientName)
    #clientFile = os.path.join(dataDir, clientName, fileString)
    #print("Checking for client path: %s" % clientPath)

    # checking for client path & spreadsheet    
    #if os.path.isdir(clientPath):
        #success = True
    #else:
        #try:
            #create_new_client(clientName)
            #success = True
        #except Exception as e:
            #print("create_new_client failed because: %s" % e)


    #if success:
        #workBook = openpyxl.load_workbook(clientFile)
    #else:
        #print("Loading template workbook :%s" % path)
        #try:
            #workBook = openpyxl.load_workbook(path)
        #except Exception as e:
            #print("Failed to load workbook: %s" % e)



# check plugins folder
if os.path.isdir(pluginDir):
    print("Plugins folder exists: %s" % os.path.isdir(pluginDir))
else:
    print("creating Plugins folder: %s" % os.path.isdir(pluginDir))
    os.makedirs(pluginDir)
# check for pricelist.xlsx
if os.path.isfile(defaultPricelist):
    print("Pricelist file exists: %s" % os.path.isfile(defaultPricelist))
else:
    print("updating Pricelist file: %s" % os.path.isfile(defaultPricelist))
    #TODO: copy from examples or from external file
print("Solar Calculator Version 0beta")

# start new session

clientPath, fileString, workBook = start_new_session()

print("Session started for %s" % clientPath)
print(workBook)

# test plugin scripts here

if do_update_price_list:
    #select a custom file
    #priceListFile = update_price_list.select()
    #create a workbook
    #workBook2 = openpyxl.load_workbook(priceListFile)
    #run the update
    #update_price_list.update(workBook2)
    
    # run the plugin
    try:
        update_price_list.update()
    except Exception as e:
        print("Could not run the plugin 'update_price_list' because: %s" % e)


if do_bom_tally:
    bom_tally.read_sheet(workBook)


def update_latitude():
    """
    check or set latitude
    
    """
    
    # the location of latitude
    sheet = "PV_Yearly"
    row = 15
    cellRef = 'B' + str(row)    
    try:
        # the yearly sheet holds the latitude setting
        workSheet = workBook.get_sheet_by_name(sheet)
        
    except Exception as e:
        print(e)
    try:
        latitude = workSheet[cellRef].value
        
    except Exception as e:
        print(e)


    if latitude:
        pass
    else:
        latitude = input_latitude()
        
    return latitude




if do_update_latitude:
    latitude = update_latitude()
    
if do_calculate_sun_angle:
    angles = pd.DataFrame(calculate_sun_angle(latitude, 24,7))
    print(angles)


def main():
    print(__name__)
    
    

if __name__ == '__main__':
    main()
else:
    print("This file should be run directly")
 
